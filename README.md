<a href="https://github.com/VitorNuness">
    <img src="https://camo.githubusercontent.com/309aabd71a71e4079eac82fa1923e37a5ef66105329deee786572d4bdc7c966b/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f4769746875622d2d626c75653f7374796c653d736f6369616c266c6f676f3d676974687562266c696e6b3d68747470733a2f2f6769746875622e636f6d2f6574746f72656c65616e64726f746f676e6f6c69" alt="Github Badge" data-canonical-src="https://img.shields.io/badge/Github--blue?style=social&amp;logo=github&amp;link=https://github.com/VitorNuness" style="max-width: 100%;">
</a>
<a href="https://gitlab.com/vitor1nuness" rel="nofollow">
    <img src="https://camo.githubusercontent.com/5641aae86e38221cac7123db2e8e70da38f90b1fa66a4fdf5ee90065407e6aae/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f4769746c61622d2d626c75653f7374796c653d736f6369616c266c6f676f3d6769746c6162266c696e6b3d68747470733a2f2f6769746c61622e636f6d2f6574746f72656c65616e64726f746f676e6f6c69" alt="Gitlab Badge" data-canonical-src="https://img.shields.io/badge/Gitlab--blue?style=social&amp;logo=gitlab&amp;link=https://gitlab.com/vitor1nuness" style="max-width: 100%;">
</a>
<a href="https://www.linkedin.com/in/vitor-nunes-da-silva-234ab3186/" rel="nofollow"><img src="https://camo.githubusercontent.com/3d3898b05a66ed9f0a7b801e9b100d5b4c7d8a3184083749a7dcc066ab7953fd/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f4c696e6b6564696e2d2d626c75653f7374796c653d736f6369616c266c6f676f3d6c696e6b6564696e266c696e6b3d68747470733a2f2f7777772e6c696e6b6564696e2e636f6d2f696e2f6574746f72652d6c65616e64726f2d746f676e6f6c692f" alt="Linkedin Badge" data-canonical-src="https://img.shields.io/badge/Linkedin--blue?style=social&amp;logo=linkedin&amp;link=https://www.linkedin.com/in/vitor-nunes-da-silva-234ab3186/" style="max-width: 100%;"></a>
<a href="https://t.me/vitornunesdasilva" rel="nofollow"><img src="https://camo.githubusercontent.com/01e31b5284a70da5a459a1a7f427f6dea769814c151acdd3a41f49c9dae28420/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f54656c656772616d2d2d626c75653f7374796c653d736f6369616c266c6f676f3d74656c656772616d266c696e6b3d68747470733a2f2f742e6d652f6574746f72656c65616e64726f746f676e6f6c69" alt="Telegram Badge" data-canonical-src="https://img.shields.io/badge/Telegram--blue?style=social&amp;logo=telegram&amp;link=https://t.me/vitornunesdasilva" style="max-width: 100%;"></a>
<a href="https://www.instagram.com/inuness01/" rel="nofollow"><img src="https://camo.githubusercontent.com/7739f72c5ef14a6d7eb34be649580e02da99f3db568d9e8c73ae46523fe3a189/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f496e7374616772616d2d2d626c75653f7374796c653d736f6369616c266c6f676f3d696e7374616772616d266c696e6b3d68747470733a2f2f7777772e696e7374616772616d2e636f6d2f6574746f72656c65616e64726f746f676e6f6c692f" alt="Instagram Badge" data-canonical-src="https://img.shields.io/badge/Instagram--blue?style=social&amp;logo=instagram&amp;link=https://www.instagram.com/inuness01/" style="max-width: 100%;"></a>
<a href="https://twitter.com/VTRNNS" rel="nofollow"><img src="https://camo.githubusercontent.com/18e61e01ce4c5a09ea816c1de555700f6614a5413d26efb1f98658ceee4348ce/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f547769747465722d2d626c75653f7374796c653d736f6369616c266c6f676f3d74776974746572266c696e6b3d68747470733a2f2f747769747465722e636f6d2f6574746f746f67" alt="Twitter Badge" data-canonical-src="https://img.shields.io/badge/Twitter--blue?style=social&amp;logo=twitter&amp;link=https://twitter.com/VTRNNS" style="max-width: 100%;"></a>

# Vitor Nunes da Silva

### Dados pessoais

- 19 de Outubro de 2001
- Marília - SP
- Assistente Financeiro

### Formação

- Analise e Desenvolvimento de Sistemas - Unimar (2023 - 2026)
- Administração - Unimar (2020 - 2022)

### Experiência profissional

- Assistente Financeiro - Tauste Supermercados Ltda (2023 - Atual)
- Auxiliar Financeiro - Tauste Supermercados Ltda (2021 - 2022)
- Operador de Caixa - Tauste Supermercados Ltda (2020 - 2021)
- Empacotador - Tauste Supermercados Ltda (2020 - 2020)

### Qualificações

- Python (iniciante)
- SQL & Banco de dados (iniciante)
- HTML & CSS (iniciante)
- Java (iniciante)
- Linux (iniciante)

---
- Pro-ativo
- Dinâmico
- Facilidade em identificar e resolver problemas
- Agilidade em operar sistemas